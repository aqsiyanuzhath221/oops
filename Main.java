import classes.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Main extends JFrame {
    private JLabel nameLabel, rollnoLabel, courseLabel, cMarksLabel, oopsMarksLabel, pythonMarksLabel, dsMarksLabel, attendanceLabel;
    private JTextField nameField, rollnoField, courseField, cMarksField, oopsMarksField, pythonMarksField, dsMarksField, attendanceField;
    private JButton submitButton;
    private JTextArea outputArea;
    private JScrollPane scrollPane;

    public Main() {
        // Initialize UI components
        nameLabel = new JLabel("Enter student name:");
        rollnoLabel = new JLabel("Enter registration number:");
        courseLabel = new JLabel("Enter course name (CSE/IT):");
        cMarksLabel = new JLabel("Enter marks in C:");
        oopsMarksLabel = new JLabel("Enter marks in OOPS:");
        pythonMarksLabel = new JLabel("Enter marks in Python:");
        dsMarksLabel = new JLabel("Enter marks in Data Structures:");
        attendanceLabel = new JLabel("Out of 150 classes, how many classes did you attend:");

        nameField = new JTextField(20);
        rollnoField = new JTextField(20);
        courseField = new JTextField(20);
        cMarksField = new JTextField(20);
        oopsMarksField = new JTextField(20);
        pythonMarksField = new JTextField(20);
        dsMarksField = new JTextField(20);
        attendanceField = new JTextField(20);

        submitButton = new JButton("Submit");
        outputArea = new JTextArea(20, 50);
        scrollPane = new JScrollPane(outputArea);

        // Set layout manager
        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.gridx = 0;
        gc.gridy = 0;
        gc.anchor = GridBagConstraints.LINE_START;
        gc.insets = new Insets(5, 5, 5, 5);
        add(nameLabel, gc);
        gc.gridx = 1;
        add(nameField, gc);
        gc.gridx = 0;
        gc.gridy = 1;
       
        add(rollnoLabel, gc);
        gc.gridx = 1;
        add(rollnoField, gc);
        gc.gridx = 0;
        gc.gridy = 2;
        add(courseLabel, gc);
        gc.gridx = 1;
        add(courseField, gc);
        gc.gridx = 0;
        gc.gridy = 3;
        add(cMarksLabel, gc);
        gc.gridx = 1;
        add(cMarksField, gc);
        gc.gridx = 0;
        gc.gridy = 4;
        add(oopsMarksLabel, gc);
        gc.gridx = 1;
        add(oopsMarksField, gc);
        gc.gridx = 0;
        gc.gridy = 5;
        add(pythonMarksLabel, gc);
        gc.gridx = 1;
        add(pythonMarksField, gc);
        gc.gridx = 0;
        gc.gridy = 6;
        add(dsMarksLabel, gc);
        gc.gridx = 1;
        add(dsMarksField, gc);
        gc.gridx = 0;
        gc.gridy = 7;
        add(attendanceLabel, gc);
        gc.gridx = 1;
        add(attendanceField, gc);
        gc.gridx = 0;
        gc.gridy = 8;
        gc.gridwidth = 2;
        gc.anchor = GridBagConstraints.CENTER;
        add(submitButton, gc);
        gc.gridx = 0;
        gc.gridy = 9;
        gc.gridwidth = 2;
        gc.fill = GridBagConstraints.BOTH;
        gc.weightx = 1;
        gc.weighty = 1;
        add(scrollPane, gc);

        // Add action listener to submit button
        submitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Get input values
                String name1 = getName();
                String name = nameField.getText();
                String rollno = rollnoField.getText();
                String course = courseField.getText();
                int cMarks = Integer.parseInt(cMarksField.getText());
                int oopsMarks = Integer.parseInt(oopsMarksField.getText());
                int pythonMarks = Integer.parseInt(pythonMarksField.getText());
                int dsMarks = Integer.parseInt(dsMarksField.getText());
                int attendance = Integer.parseInt(attendanceField.getText());

                // Create objects and calculate values
                Attendance attendanceObj = new Attendance();
                boolean isValid = attendanceObj.Edit();
                attendanceObj.presence = isValid;
                double attPer = attendanceObj.per;
                Student student = new Student();
                student.Album_Number = rollno;
                boolean isValidRollno = student.proEdit();
                while (!isValidRollno) {
                    rollno = JOptionPane.showInputDialog("Please reenter your registration number as the entered one is not valid:");
                    student.Album_Number = rollno;
                    isValidRollno = student.proEdit();
                }
                Course courseObj = new Course();
                courseObj.CoureseName = course;
                Final_grade finalGrade = new Final_grade(courseObj);
                String passStatus = finalGrade.pass ? "PASS" : "FAIL";

                // Output result to text area
                outputArea.append(name + "\t" + rollno + "\t" + (isValid ? "valid" : "NOT valid") + "\t" + attPer + "\t" + course + "\t" + finalGrade.grade + "\t" + passStatus + "\t" + attendance + "\n");

                // Display popup window with subject marks
                String subjectMarks = "Subject Marks:\n";
                subjectMarks += "C: " + cMarks + "\n";
                subjectMarks += "OOPS: " + oopsMarks + "\n";
                subjectMarks += "Python: " + pythonMarks + "\n";
                subjectMarks += "Data Structures: " + dsMarks + "\n";
                JOptionPane.showMessageDialog(Main.this, subjectMarks, "Subject Marks", JOptionPane.INFORMATION_MESSAGE);

                // Create a new dialog for displaying the output
                JDialog outputDialog = new JDialog(Main.this, "Output");
                JTextArea outputTextArea = new JTextArea();
                outputTextArea.setEditable(false);
                outputTextArea.setText(outputArea.getText());
                JScrollPane outputScrollPane = new JScrollPane(outputTextArea);
                outputDialog.getContentPane().add(outputScrollPane);
                outputDialog.setSize(500, 400);
                outputDialog.setLocationRelativeTo(null);
                outputDialog.setVisible(true);
            }
        });

        // Set frame properties
        setTitle("Student Evaluation System");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        // Create GUI on the event dispatch thread
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Main();
            }
        });
    }
}
