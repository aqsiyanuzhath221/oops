#include<iostream>
using namespace std;
class Student{
    private:
    string college_name;
    int college_code;

    public:
    //default constructor
    Student()
    {
        cout<<"Default constructor: "<<endl;
        college_name = "MVGR";
        college_code = 250023;
    }

    void display() {
        cout<<"College name is: "<<college_name<<endl;
        cout<<"College code is: "<<college_code<<endl;
    }
    ~Student(){
        cout<<"Thank you"<<endl;
    }
};

class student{
    private:
    string fullName;
    double semPercentage;

    public:
    //parameterized constructor
    student(string student_name, double percentage)
    {
                //cout<<"FullName is :"<<endl;
        fullName = student_name;
        semPercentage = percentage;
    }

    void display()
    {
        cout<<"Full name of the student is :"<<fullName<<endl;
        cout<<"Semester percentage of the student is :"<<semPercentage<<endl;
    }
    ~student(){
        cout<<"Welcome"<<endl;
    }
};

int main() 
{
    // creating object of class using default constructor
    Student obj;
    
    // printing class data members 
    obj.display();

    student obj1("Aqsiya",90);
    obj1.display();
    
    return 0;
}


