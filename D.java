class A {
    void disp() {
        System.out.println("ANDHRA PRADESH");
    }
}

class B extends A {

    void disp1() {
        System.out.println("TELANGANA");
    }
}

class C extends B {

    void disp2() {
        System.out.println("KARNATAKA");
    }

    class D extends B,C{

        void disp3() {
            System.out.println("TAMILNADU");

        }

        public static void main(String args[]) {
            D obj = new D();
            obj.disp();
            obj.disp1();
            // obj.disp2();
            obj.disp3();
        }
    }
}