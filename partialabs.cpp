

#include <iostream>
using namespace std;
  
class Abstraction
{
    private:
        string x, y;
  
    public:
      
          void set(string a, string b)
        {
            x = a;
            y = b;
        }
        void print()
        {
            cout<<"x = " << x << endl;
            cout<<"y = " << y << endl;
        }
};
  
int main() 
{
    Abstraction t1;
    t1.set("string a", "string b");
    t1.print();
    
    return 0;
}

