package pack;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class CollectionsDemo {
    public static void main(String[] args) {
        // List Example
        List<String> names = new ArrayList<>();
        names.add("minato");
        names.add("naruto");
        names.add("boruto");


        System.out.println("List Example:");
        for (String name : names) {
            System.out.println(name);
        }


        // Set Example
        Set<Integer> numbers = new HashSet<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(2); // Duplicates are automatically removed in a Set


        System.out.println("\nSet Example:");
        for (int number : numbers) {
            System.out.println(number);
        }


        // Map Example
        Map<String, Integer> scores = new HashMap<>();
        scores.put("minato", 90);
        scores.put("naruto", 80);
        scores.put("boruto", 95);


        System.out.println("\nMap Example:");
        for (Map.Entry<String, Integer> entry : scores.entrySet()) {
            String name = entry.getKey();
            int score = entry.getValue();
            System.out.println(name + ": " + score);
        }
    }
}

