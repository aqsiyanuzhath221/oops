#include<iostream>
using namespace std;
class parent{
    public:
    virtual void features()=0;
};
class child1:public parent{
    public:
    void features(){
        cout<<"child1 has features of mother"<<endl;
    }
};
class child2:public parent{
    public:
    void features(){
        cout<<"child 2 has features of father"<<endl;
    }
};
int main(){
  child1 obj;
  child2 obj1;
  obj.features();
  obj1.features();

}