public class BoxingDemo {
  public static void main(String[] args) {
    int x = 21;
    Integer y = Integer.valueOf(x);
    Integer z = x;
    Integer a = new Integer(16);
    int b = a.intValue();
    int c = a;
    double u = 22.00;
    Double v = Double.valueOf(u);
    Double w = u;
    System.out.println("Boxing: x=" + x + ", y=" + y + ", z=" + z);
    System.out.println("Unboxing: a=" + a + ", b=" + b + ", c=" + c);
    System.out.println("Boxing: u=" + u + ", v=" + v + ", w=" + w);
  }
}
