package aqsiya;
public class Fib{
    public void fibseries(long n){
        long n0=0,n1=0,n2=1,i;   
        System.out.print("The "+n+"th Fibonacci Series is : "); 
        System.out.print(n1+" "+n2);            
        for(i=2;i<n;++i)
        {    
         n0=n1+n2;    
         System.out.print(" "+n0);    
         n1=n2;    
         n2=n0;    
        }    
        System.out.print("\n");    
    }
    public void fibnum(long n){
        long n0=0,n1=0,n2=1,i;    
        if (n==1){
            System.out.println("The "+n+" number of Fibonacci is : 0"); 
        }
        else if (n==2) {
            System.out.println("The "+n+" number of Fibonacci is : 1"); 
        }
        else{
        for(i=2;i<n;++i)
        {    
         n0=n1+n2;    
         n1=n2;    
         n2=n0;    
        }
        System.out.println("The "+n+" number of Fibonacci is : "+n0);
        }
    }
}