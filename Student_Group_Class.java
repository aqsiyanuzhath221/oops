package classes;
import java.util.Scanner;
public class Student_Group{
	public String Group_Name;
	public long Yearbook;
	Scanner sc = new Scanner(System.in);
	public Student_Group(){
		System.out.println("Enter the year of batch = ");
		Yearbook = sc.nextInt();
		System.out.println("Enter Group name (cse - a,b 0r c) = ");
		Group_Name = sc.next();
	}
	public void Edit(){
		System.out.println("Do you want to change year of batch press(y/n) = ");
		char a = sc.next().charAt(0);
		if (a == 'y'){
			System.out.println("Enter the year of batch = ");
			Yearbook = sc.nextInt();
		}
	}
	public long StudentCount(){
		System.out.println("Enter the no of students in your group = ");
		long b = sc.nextInt();
		return b;
	}
	public long MaxCount(){
		System.out.println("Enter the max. no. of students possible in your group = ");
		long c = sc.nextInt();
		return c;
	}
}