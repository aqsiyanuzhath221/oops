package classes;
import classes.Grade;
import classes.Partial_grade;
public class Final_grade extends Grade{
	public boolean pass;
	public String grade;
	public Final_grade(Course obj){
		super(obj);
		Partial_grade p = new Partial_grade(obj);
		double per = ((Value + p.Notes)*100)/500;
		if (per >= 90){
			grade = "A";
			pass = true;
		}
		else if(per >= 80 && per <90){
			grade = "B";
			pass = true;
		}
		else if(per >= 70 && per <80){
			grade = "C";
			pass = true;
		}
		else if(per >= 60 && per <70){
			grade = "D";
			pass = true;
		}
		else if(per >= 50 && per <60){
			grade = "E";
			pass = true;
		}
		else if(per < 50){
			grade = "F";
			pass = false;
		}
	}
}