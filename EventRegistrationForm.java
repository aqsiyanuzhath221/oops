import java.awt.*;

import java.awt.event.*;



public class EventRegistrationForm extends Frame implements ActionListener {

    private Label nameLabel;

    private Label emailLabel;

    private Label eventLabel;

    private TextField nameField;

    private TextField emailField;

    private Choice eventChoice;

    private Button registerButton;



    public EventRegistrationForm() {

        setLayout(new GridLayout(4, 2, 10, 10));



        nameLabel = new Label("Name:");

        add(nameLabel);



        nameField = new TextField(20);

        add(nameField);



        emailLabel = new Label("Email:");

        add(emailLabel);



        emailField = new TextField(20);

        add(emailField);



        eventLabel = new Label("Event:");

        add(eventLabel);



        eventChoice = new Choice();

        eventChoice.add("Birthday Party");

        eventChoice.add("Wedding");

        eventChoice.add("Corporate Event");

        add(eventChoice);



        registerButton = new Button("Register");

        add(registerButton);

        registerButton.addActionListener(this);



        setTitle("Event Registration Form");

        setSize(400, 200);

        setVisible(true);

    }



    public void actionPerformed(ActionEvent e) {

        String name = nameField.getText();

        String email = emailField.getText();

        String event = eventChoice.getSelectedItem();



        System.out.println("Name: " + name);

        System.out.println("Email: " + email);

        System.out.println("Event: " + event);



    }



    public static void main(String[] args) {

        EventRegistrationForm form = new EventRegistrationForm();

    }

}

