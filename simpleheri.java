// Superclass or Base class
class Animal {
    void eat() {
       System.out.println("Eating...");
    }
 }
 
 // Subclass or Derived class
 class Dog extends Animal {
    void bark() {
       System.out.println("Barking...");
    }
 }
 
 // Main class
 class Main {
    public static void main(String[] args) {
       // Creating an object of subclass
       Dog dog = new Dog();
 
       // Calling methods of superclass and subclass
       dog.eat();
       dog.bark();
    }
 }
 