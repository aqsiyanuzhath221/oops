#include<iostream>
using namespace std;
class Parent{
   public:
   void principle() {
       cout<<"YOU SHOULD STUDY WELL!"<<endl;
   }
   void teacher() {
       cout<<"YOU SHOULD GET GOOD MARKS!"<<endl;
   }
};
class child : public Parent{
   public:
   void student() {
       cout<<"I WILL STUDY AND GET GOOD MARKS." <<endl;
   }
};
int main() {
   child obj;
   obj.principle();
   obj.teacher();
   obj.student();
   return 0;
}
