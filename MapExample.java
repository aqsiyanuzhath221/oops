import java.util.HashMap;
import java.util.Map;


public class MapExample {
    public static void main(String[] args) {
        // Creating a Map
        Map<String, Integer> scores = new HashMap<>();


        // Adding elements to the Map
        scores.put("Alice", 90);
        scores.put("Bob", 80);
        scores.put("Charlie", 95);


        // Accessing elements from the Map
        int aliceScore = scores.get("Alice");
        System.out.println("Alice's score: " + aliceScore);


        // Updating elements in the Map
        scores.put("Bob", 85);


        // Removing elements from the Map
        scores.remove("Charlie");


        // Checking if a key exists in the Map
        boolean containsKey = scores.containsKey("Bob");
        System.out.println("Contains Bob? " + containsKey);


        // Iterating over the Map entries
        System.out.println("Map entries:");
        for (Map.Entry<String, Integer> entry : scores.entrySet()) {
            String name = entry.getKey();
            int score = entry.getValue();
            System.out.println(name + ": " + score);
        }


        // Clearing the Map
        scores.clear();


        // Checking if the Map is empty
        boolean isEmpty = scores.isEmpty();
        System.out.println("Is the map empty? " + isEmpty);
    }
}

