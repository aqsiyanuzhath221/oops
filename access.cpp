#include<iostream>
using namespace std;

class AccessSpecifierDemo {
private:
    int priVar;
protected:
    int proVar;
public:
    int pubVar;
    
    void setVar(int priValue, int proValue, int pubValue) {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }
    
    void getVar() {
        cout << "Private Variable: " << priVar << endl;
        cout << "Protected Variable: " << proVar << endl;
        cout << "Public Variable: " << pubVar << endl;
    }
};

int main() {
    AccessSpecifierDemo obj;
    obj.setVar(21, 17, 26);
    obj.getVar();
    return 0;
}
