import java.security.Identity;
public class IdentityExample {
    public static void main(String[] args) {
        // Create two identities with the same name
        Identity id1 = new Identity("Aqsiya");
        Identity id2 = new Identity("Nuzhath");
        
        // Print out the info for each identity
        System.out.println("id1.getInfo(): " + id1.getInfo());
        System.out.println("id2.getInfo(): " + id2.getInfo());
        
        // Print out the name for each identity
        System.out.println("id1.getName(): " + id1.getName());
        System.out.println("id2.getName(): " + id2.getName());
        
        // Print out the scope for each identity
        System.out.println("id1.getScope(): " + id1.getScope());
        System.out.println("id2.getScope(): " + id2.getScope());
        
        // Check if the two identities are equal
        boolean areEqual = id1.equals(id2);
        System.out.println("id1.equals(id2): " + areEqual);
    }
}
